# IBM 5140 XT-IDE

This is a repository of hopes and dreams, not quite an implementation. Please if you have some technical knowledge and would be interested throw me a message!

I'm wanting to replace the IBM5140's modem/battery box with a custom XT-IDE adapter or similar.

I feel like that as a primary goal is quite achievable.

## Secondary goals

 * Adlib compatible sound device.
 * Raspberry PI W interface.
 * Battery Replacement Lipo/18650/USB-C charge/power system
 * Battery backed RTC?

Given the placement of the modem and it's attachment to the battery box, it could be replaced entirely providing "lots of rooms for activities!" I'd imagine that nearly all of the OEM batteries are entirely busted by now.  It could be possible to replace the entire battery/modem component with a 3D Printed replacement using 18650's and a charge controller, giving a nice upgrade to the system. An access port for a USB-C power/charging port could go where the modem port currently exists, and access to the compact flash slot would be from the battery compartment.  

The secondary goals would be nice to haves, as I could augment the sound capabilities and upgrade the power/processing.   One could open a terminal into the raspberry pi and run modern software on the display over TTY.


## What I Have

Currently I only have a few reference material, and some vague sense of what needs to be done.  I cloned http://ohlandl.ipv7.net/5140/5140.html to /reference/5140.html as it has lots of information on the 5140 including a pinout for the internal modem connections.  And wanted to make sure an archive of it survived.

### Internal Modem Port Pinout


| I/O Pin | Signal Name | I / O |
| ------- | ----------- | ----- |
| 01 | Adress/Data Bit 0 | I/O |
| 02 | Adress/Data Bit 1 | I/O |
| 03 | Adress/Data Bit 2 | I/O |
| 04 | Adress/Data Bit 3 | I/O |
| 05 | Adress/Data Bit 4 | I/O |
| 06 | Adress/Data Bit 5 | I/O |
| 07 | Adress/Data Bit 6 | I/O |
| 08 | Adress/Data Bit 7 | I/O |
| 09 | Adress Bit 8      | I   |
| 10 | Adress Bit 9      | I   |
| 11 | Interupt Request 4| O   |
| 12 | - I/O Read        | I   |
| 13 | + Reset           | I   | 
| 14 | - Data Enable     | I   |
| 15 | Ground | GND |
| 16 | + Adress Latch Enable | I |
| 17 | Ground | GND |
| 18 | + Adress Enable | I |
| 19 | Ground | GND |
| 20 | + 12 VDC | PWR |
| 21 | Ground | GND |
| 22 | Ground | GND |
| 23 | Ground | GND |
| 24 | Ground | GND |
| 25 | - 13 VDC | PWR |
| 26 | Ground | GND |
| 27 | - I/O Write | I |
| 28 | + 5 VDC | PWR |
| 29 | Ground | GND |
| 30 | + High Z | I |


* The bus interface is multiplexed with data


Some working assumptions I'm making ATM:

> Address Latch goes high to signal latching of the address lines. I don't know if the latch needs to save all 10 or just the lower multiplexed 8 bits of the address data, I'll get a logic analyzer on soon and test it out.

> After latching the address lines, the High Z line goes high telling you to go high Z on the multiplexed lines

> The Read/Write lines will be pulled low

> For a read action, the Data enable will go low ? 

> For a write action, the data will be present on the lines when the Write line goes low.

Assuming the above is true:

The address latching mechanism matches the logic of a "74374" quite handily.

And the data lines match the mechanism of the "74241" a tri-state octal bus driver.

I haven't worked out much more than this, but I figured if I post my musing, I might inspire more work or tips from people who really understand this stuff.

## Musings

### Can I interface a microcontroller through this bus?

I wondered about using an ATMEGA328 to interface with this? Would running it at 20mhz maybe be fast enough on this data bus to intercept signals reliably and bi-directionally? I think using a chip like that would be subject to a lot of bit wrangling in software, I'd imagine you'd use up most of your clock cycles just trying to reassemble the data in a logical pattern to THEN do actual work on it.

Although I suspect a part like 600mhz teensy 4.0 could do the deed in software, the issue there is the 5v signalling, the extra components may defeat the purpose of "trying to do it in software anyway"?


## ROADMAP

Right now I have broken down what I need to do in very vague steps.  I'm a tinkerer more than an electronics engineer, so I'm breaking it down into chunks I know I can manage.  I will also be moving rather slow on this project as it's one of my millions I seem to have! (Unless others are interested in helping out)

  * Get breakout ribbon cable attached (header/ribbon is on it's way 17/4/21)
  * Logic analyzer (I was thinking a basic one like the 8ch atmega based ones would work just fine for this, due to the bus speed... I hope I'm not wrong!)
  * Verification of the pinout!! 
  * Basic addr/data multiplex decoding - getting the address data into a register, and using a magnitude comparator (74688/785) I found an example schematic for the 7485 for an exact 10 bit comparator https://www.ques10.com/p/6230/implement-10-bit-comparator-using-ic-7485-1/
  * Write Software that spews data to an unused port, so I can detect an address with the magnitude comparator.
  * Write Some software that spews data that gets loaded into a register (I think I have an 8 bit dual 7 seg hex display somewhere might be a fun to plug into that)
  * Tackle reading data from the port multiplexing.  I think I could use the 74241 backed by a 8 pin dip switch for testing.
  * Start considering how to make the bare metal bits for the IDE interface** (nebulous and vague)

I've left this very open and described my thoughts in the hope that if I never finish, someone else would be inspired by how easy it seems, OR someone who know how wrong I am can interject and tell me so!

# UPDATES

## 17/4/21
(SCRATCH THIS!, IRQ's are not necessary for basic PIO and simplest interface!!!) First critical issue, this is kind of an "oh duh!" moment - There is only one IRQ line availble to the port IRQ 4, I suspect this won't be an outright show stopper, as I could probably modify the XT-IDE firmware. Some software may just outright blow the interrupt handler out for that (terminal software?)  Could have some incompatibilities, which I'm happy to live with honestly, as long as it works. 

